package com.sel.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by amitdatta on 07/12/16.
 */
public class FacebookLogin {

    @FindBy(id="email")
    private WebElement un;

    @FindBy(id="pass")
    private WebElement pw;


    public FacebookLogin(WebDriver driver){

        PageFactory.initElements(driver,this);
    }

    public void setEmail(String a){
        un.sendKeys(a);
    }
    public void setPwd(String ab){
        pw.sendKeys(ab);
    }

}
