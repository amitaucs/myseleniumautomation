package com.sel.scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

/**
 * Created by amitdatta on 07/12/16.
 */


public class BaseClass {

    public WebDriver driver;

    @BeforeClass
    public void preCondition(){


        driver = new FirefoxDriver();
        driver.get("https://www.facebook.com/");

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @AfterClass
    public void postCondition(){

        driver.close();
    }

}
