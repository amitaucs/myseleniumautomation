package com.sel.scripts;

import org.testng.Reporter;
import org.testng.annotations.*;

/**
 * Created by amitdatta on 07/12/16.
 */

//@Test
public class TestNG1 {

    @BeforeClass
    public void login()
    {
        Reporter.log("login",true);
    }

    @AfterClass
    public void logout(){
        Reporter.log("kvaya",true);
    }

    @BeforeMethod
    public void openApp(){
        Reporter.log("openapp",true);
    }
    @AfterMethod
    public void closeApp(){
        Reporter.log("closeApp",true);
    }

    @Test(priority = -1,invocationCount = 2)
    public void about(){
        Reporter.log("about",true);
    }

    @Test(priority = 1)
    public void delete(){
        Reporter.log("delete",true);
    }

    @Test
    public void user(){
        Reporter.log("user",true);
    }





}
